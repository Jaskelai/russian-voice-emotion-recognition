from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.set_xlabel("Активация")
ax.set_ylabel("Валентность")
ax.set_zlabel("Доминация")
# xdata = [0] * 40
# zdata = [-2] * 40
# ydata = [-2] * 40
# ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens')

coefs = (2, 2, 2)  # Coefficients in a0/c x**2 + a1/c y**2 + a2/c z**2 = 1
# Radii corresponding to the coefficients:
rx, ry, rz = 1/np.sqrt(coefs)

# Set of all spherical angles:
u = np.linspace(0, 2 * np.pi, 100)
v = np.linspace(0, np.pi, 100)

# Cartesian coordinates that correspond to the spherical angles:
# (this is the equation of an ellipsoid):
x = rx * np.outer(np.cos(u), np.sin(v))
y = ry * np.outer(np.sin(u), np.sin(v)) - 0.25
z = rz * np.outer(np.ones_like(u), np.cos(v)) - 0.25

# Plot:
ax.plot_surface(x, y, z,  rstride=4, cstride=4, color='b')
# Adjustment of the axes, so that they all have the same span:
max_radius = max(rx, ry, rz)
for axis in 'xyz':
    getattr(ax, 'set_{}lim'.format(axis))((-max_radius, max_radius))
plt.title = "Грусть"

plt.show()