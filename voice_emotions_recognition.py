import librosa
import librosa.display
import soundfile as sff
import matplotlib.pyplot as plt
import numpy as np
import os, glob, pickle
import pandas as pd
import random

from pydub import AudioSegment
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Flatten, Dropout, Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.utils import to_categorical

audio_path = "raw//Actor_*//*.wav"

# optional: load audio file for specs
# for file in glob.glob(audio_path):
#     file_path = os.path.basename(file)
# y, sr = librosa.load("raw/Actor_25//03-01-02-02-02-02-01.wav")

# optional: Plot MFCCs
# fig = plt.figure(figsize=(20, 10))
# Y = librosa.feature.mfcc(y)
# Ydb = librosa.amplitude_to_db(abs(Y))
# i = librosa.display.specshow(Ydb, x_axis='time')
# plt.colorbar(i)
# plt.show()

# optional: Plot Waveform
# plt.figure(figsize=(10, 4))
# librosa.display.waveplot(y, sr=sr)
# plt.title('Форма волны')
# plt.ylabel("Амплитуда")
# plt.xlabel("Время")
# plt.show()

# optional: Spectogram
# Y = librosa.stft(y)
# Ydb = librosa.amplitude_to_db(abs(Y))
# plt.figure(figsize=(14, 5))
# librosa.display.specshow(Ydb, sr=sr, x_axis='time', y_axis='hz')
# plt.xlabel("Время")
# plt.ylabel("Частота, Гц")
# plt.title('Спектрограмма')
# plt.colorbar()
# plt.show()
#
# optional: MelSpectogram
# spectrogram = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=128, fmax=8000)
# spectrogram = librosa.power_to_db(spectrogram)
# librosa.display.specshow(spectrogram, y_axis='mel', fmax=8000, x_axis='time')
# plt.title('Mel Spectrogram - Male Neutral')
# plt.savefig('MelSpec_MaleNeutral.png')
# plt.colorbar(format='%+2.0f dB')

emotion_labels = {
    '01': 'нейтральность',
    '03': 'счастье',
    '04': 'грусть',
    '05': 'злость',
    '06': 'страх',
    '08': 'удивление'
}


def padding(array, xx, yy):
    h = array.shape[0]
    w = array.shape[1]
    a = max((xx - h) // 2, 0)
    aa = max(0, xx - a - h)
    b = max(0, (yy - w) // 2)
    bb = max(yy - b - w, 0)
    return np.pad(array, pad_width=((a, aa), (b, bb)), mode='constant')


def label_to_num(array):
    array = [0 if x == 'нейтральность' else x for x in array]
    array = [1 if x == 'счастье' else x for x in array]
    array = [2 if x == 'грусть' else x for x in array]
    array = [3 if x == 'злость' else x for x in array]
    array = [4 if x == 'страх' else x for x in array]
    array = [5 if x == 'удивление' else x for x in array]
    return array


def get_audio_features_from_file(file_title):
    max_size = 16000  # my max audio file feature width
    n_fft = 255  # window in num. of samples
    sound_loaded = AudioSegment.from_wav(file_title)
    sound_loaded = sound_loaded.set_channels(1)
    sound_loaded.export(file_title, format="wav")
    with sff.SoundFile(file_title) as audio_recording:
        audio_open = audio_recording.read(dtype="float32")
        sample_rate = audio_recording.samplerate
        stft = padding(np.abs(librosa.stft(audio_open, n_fft=n_fft, hop_length=512)), 128, max_size)
        mfccs_features = padding(librosa.feature.mfcc(audio_open, n_fft=n_fft, hop_length=512, n_mfcc=128), 128,
                                 max_size)
        mel_features = librosa.feature.melspectrogram(audio_open, sr=sample_rate)
        chroma_features = librosa.feature.chroma_stft(S=stft, sr=sample_rate)
        image = np.array([padding(normalize(mel_features), 1, max_size)]).reshape(1, max_size)
        for i in range(0, 9):
            image = np.append(image, padding(normalize(chroma_features), 12, max_size), axis=0)
            image = np.append(image, padding(normalize(mel_features), 1, max_size), axis=0)
        image = np.dstack((image, np.abs(stft)))
        image = np.dstack((image, mfccs_features))
        return image


def loading_audio_data():
    x = []
    y = []

    extract_features_from_audio("raw//Actor_*//*.wav", x, y)

    final_dataset = train_test_split(np.array(x), y, test_size=0.1, random_state=9)
    save_by_pickle(final_dataset, "extracted_features")
    return final_dataset


def extract_features_from_audio(file_pattern, x, y):
    for file in glob.glob(file_pattern):
        file_path = os.path.basename(file)
        try:
            emotion = emotion_labels[file_path.split("-")[2]]
        except KeyError:
            continue
        feature = get_audio_features_from_file(file)

        x.append(feature)
        y.append(emotion)


def calculate_neighbours(predictions_sorted, percent_float):
    max_value = predictions_sorted[0]
    max_value_percent = max_value * percent_float
    idxes = []
    for idx, prediction in predictions_sorted:
        if prediction < max_value_percent:
            idxes.append(prediction)
    return idxes


def handle_predictions(x, y, model_pred):
    result = []
    for idx, val in x:
        test = X_test[idx]
        test = np.expand_dims(test, axis=0)
        predictions = model_pred.predict(test)
        predictions.sort(reverse=True)
        print(predictions)
        idxes_neighbours = calculate_neighbours(predictions, 0.03)
        res_value = predictions[0]
        if idxes_neighbours.__contains__(1):
            res_value = 1
        if idxes_neighbours.__contains__(0):
            res_value = 0
        return res_value


def save_by_pickle(data, filename):
    pickle.dump(data, open(filename, 'wb'))


def read_by_pickle(filename):
    return pd.read_pickle(filename)


# y_train = label_to_num(y_train)
# y_test = label_to_num(y_test)
#
# y_train = to_categorical(y_train, 6)
# y_test = to_categorical(y_test, 6)
#
# X_train = np.array((X_train - np.min(X_train)) / (np.max(X_train) - np.min(X_train)))
# X_test = np.array((X_test - np.min(X_test)) / (np.max(X_test) - np.min(X_test)))
# X_train = X_train / np.std(X_train)
# X_test = X_test / np.std(X_test)
# y_train = np.array(y_train)
# y_test = np.array(y_test)

# show diagrams
# for emotion in emotion_labels.items():
#     indexes_of_emotion = [i for i, val in enumerate(y_train) if val == 'neutral']
#     values_of_emotion = [res_list[index] for index in indexes_of_emotion]
#     sns.displot(values_of_emotion, kde=True, x=emotion)
#     plt.show()


# array = []
# emotion_labels_final = {
#     '01': 'neutral',
#     '02': 'happy',
#     '03': 'sad',
#     '04': 'angry',
#     '05': 'fearful',
#     '06': 'surprised'
# }
# emotion_labels_final_rus = {
#     '01': 'нейтральность',
#     '02': 'радость',
#     '03': 'грусть',
#     '04': 'злость',
#     '05': 'страх',
#     '06': 'удивление'
# }
# for emotion in emotion_labels_final.items():
#     indexes_of_emotion = [i for i, val in enumerate(y_train) if val == emotion[1]]
#     values_of_emotion = [res_list[index] for index in indexes_of_emotion]
#     # df_describe = pd.DataFrame(values_of_emotion)
#     # res = df_describe.describe()
#     array.append(values_of_emotion)
# fig, ax = plt.subplots()
# plt.tight_layout()
# plt.boxplot(array, labels=emotion_labels_final_rus.values())
# ax.set_title("Chroma")
# plt.show()

# test approach
# model.add(Conv2D(64, kernel_size=(3, 3), strides=(1, 1), activation='relu', input_shape=(1695, 3, 1)))
# model.add(MaxPool2D(pool_size=(1,1)))
# # flatten output of conv
# model.add(Flatten())
# # hidden layer
# model.add(Dense(32, activation='relu'))
# # output layer
# model.add(Dense(6, activation='softmax'))

# comment if 'extracted_features' already exist for time boost
# X_train, X_test, y_train, y_test = loading_audio_data()

X_train, X_test, y_train, y_test = read_by_pickle("extracted_features")

y_train = label_to_num(y_train)
y_test = label_to_num(y_test)

y_train = to_categorical(y_train, 6)
y_test = to_categorical(y_test, 6)

X_train = np.array((X_train - np.min(X_train)) / (np.max(X_train) - np.min(X_train)))
X_test = np.array((X_test - np.min(X_test)) / (np.max(X_test) - np.min(X_test)))
X_train = X_train / np.std(X_train)
X_test = X_test / np.std(X_test)
y_train = np.array(y_train)
y_test = np.array(y_test)

# final approach
input_shape = (256, 16000, 3)
model = Sequential()
model.add(Conv2D(32, (3, 3), activation='relu', input_shape=input_shape))
model.add(MaxPooling2D((2, 2)))
model.add(Dropout(0.3))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D((2, 2)))
model.add(Dropout(0.2))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(Flatten())
model.add(Dense(32, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(12, activation='relu'))
model.add(Dense(6, activation='softmax'))

# model.summary()
model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')
model.fit(X_train, y_train, epochs=2850, validation_data=(X_test, y_test))
save_by_pickle(model, "model_cnn")

handle_predictions(X_test, y_test, model)
